using UnityEngine;

public class CombinedCameraMovement : MonoBehaviour
{
    public Transform targetWorld;
    public float translationDistance = 5f;
    public float velocity = 1f;
    public float acceleration = 1f;
    public float stopMove = 22f;

    private Vector3 targetPosition;
    private float currentVelocity;
    private bool isCameraMoving = true;

    private void Start()
    {
        targetPosition = transform.position + (targetWorld.position - transform.position).normalized * translationDistance;
        currentVelocity = velocity;
        Invoke("StopCameraMovement", stopMove);
    }

    private void Update()
    {
        if (!isCameraMoving)
            return;

        Vector3 direction = (targetWorld.position - transform.position).normalized;
        float movement = currentVelocity * Time.deltaTime;
        Vector3 movementVector = new Vector3(direction.x, 0f, direction.z) * movement;

        transform.Translate(movementVector);

        currentVelocity += acceleration * Time.deltaTime;
    }

    private void StopCameraMovement()
    {
        isCameraMoving = false;
    }

    public void PauseCameraMovement()
    {
        isCameraMoving = false;
    }

    public void ResumeCameraMovement()
    {
        isCameraMoving = true;
    }
}
