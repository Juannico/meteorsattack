using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    public float delay = 19f;
    public float delayDesactive = 21f;
    private CinemachineFreeLook freeLookCamera;
    private CinemachineOrbitalTransposer orbitalTransposer;

    private void Start()
    {
        freeLookCamera = GetComponent<CinemachineFreeLook>();
        orbitalTransposer = freeLookCamera.GetRig(0).GetCinemachineComponent<CinemachineOrbitalTransposer>();
        Invoke("AssignPlayerToCamera", delay);
        Invoke("DisableCameraFollow", delayDesactive);
    }

    private void AssignPlayerToCamera()
    {
        freeLookCamera.Follow = player.transform;
        LimitCameraOrbit();
    }

    private void DisableCameraFollow()
    {
        freeLookCamera.Follow = null;
    }

    private void LimitCameraOrbit()
    {
        // Aqu� puedes ajustar los l�mites de las �rbitas de la c�mara seg�n tus necesidades.
        // Establece los valores m�nimos y m�ximos para el eje X (horizontal) y el eje Y (vertical) de las �rbitas.
        orbitalTransposer.m_XAxis.m_MaxValue = 60f;  // L�mite m�ximo de �rbita horizontal
        orbitalTransposer.m_XAxis.m_MinValue = -60f; // L�mite m�nimo de �rbita horizontal
                                             
    }
}
