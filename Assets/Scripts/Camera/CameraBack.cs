using Cinemachine;
using UnityEngine;

public class CameraBack : MonoBehaviour
{
    public PlayerScore playerScore;
    public CinemachineFreeLook freeLookCamera;
    public CameraController movementScript;
    public CombinedCameraMovement combinedCameraMovement;
    public GameObject player;

    private bool isMovedToPosition900 = false;
    private float followDuration = 2f;

    private void Update()
    {
        if (playerScore.score >= 2000 && playerScore.elapsedTime >= playerScore.multipliedScoreTime && !isMovedToPosition900)
        {
            MovePlayerToPosition900Z();
        }
    }

    private void MovePlayerToPosition900Z()
    {
        if (freeLookCamera != null)
        {
            Vector3 newPosition = freeLookCamera.transform.position;
            newPosition.z = 704f;
            freeLookCamera.transform.position = newPosition;

            if (movementScript != null)
            {
                movementScript.enabled = true;
            }

            Invoke("AddCameraFollow", 1f); // A�adir follow despu�s de un segundo

            isMovedToPosition900 = true;

            combinedCameraMovement.PauseCameraMovement();

            Invoke("ResumeCameraMovement", 0f);
            Invoke("DisableCameraFollow", followDuration);
        }
    }

    private void AddCameraFollow()
    {
        if (freeLookCamera != null && player != null)
        {
            freeLookCamera.Follow = player.transform;
        }
    }

    private void ResumeCameraMovement()
    {
        if (combinedCameraMovement != null)
        {
            combinedCameraMovement.ResumeCameraMovement();
        }

        Invoke("PauseCameraMovement", 2.5f);
    }

    private void PauseCameraMovement()
    {
        if (combinedCameraMovement != null)
        {
            combinedCameraMovement.PauseCameraMovement();
        }
    }

    private void DisableCameraFollow()
    {
        freeLookCamera.Follow = null;
    }
}
