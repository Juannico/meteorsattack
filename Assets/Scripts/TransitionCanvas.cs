using UnityEngine;
using UnityEngine.UI;

public class TransicionScript : MonoBehaviour
{
    public Image imagenTransicion;
    public float duracionTransicion = 2f;
    public Color colorInicio;
    public Color colorFinal;
    public float puntoZ = 20f; // momento activa transicion
    private bool transicionActiva = false;
    

    private void Start()
    {
        imagenTransicion.gameObject.SetActive(false);
        imagenTransicion.color = colorInicio;
    }

    private void Update()
    {
        if (!transicionActiva && Time.timeSinceLevelLoad >= puntoZ)
        {
            StartCoroutine(ActivarTransicion());
        }
    }

    private System.Collections.IEnumerator ActivarTransicion()
    {
        transicionActiva = true;
        imagenTransicion.gameObject.SetActive(true);

        float tiempoPasado = 0f;
        while (tiempoPasado < duracionTransicion)
        {
            float t = tiempoPasado / duracionTransicion;
            imagenTransicion.color = Color.Lerp(colorInicio, colorFinal, t);
            tiempoPasado += Time.deltaTime;
            yield return null;
        }

        imagenTransicion.color = colorFinal;
        yield return new WaitForSeconds(0.5f); // Espera medio segundo antes de desactivar la imagen

        imagenTransicion.gameObject.SetActive(false);
    }
}
