using UnityEngine;

public class LaserPointer : MonoBehaviour
{
    public Material laserMaterial;
    private GameObject laserDecal;

    private void Start()
    {
        laserDecal = new GameObject("LaserDecal");
        laserDecal.transform.parent = transform;
        laserDecal.transform.localPosition = Vector3.zero;
        laserDecal.transform.localRotation = Quaternion.identity;

        MeshFilter meshFilter = laserDecal.AddComponent<MeshFilter>();
        MeshRenderer meshRenderer = laserDecal.AddComponent<MeshRenderer>();

        meshRenderer.material = laserMaterial;
    }

    private void Update()
    {
        // Obtener la direcci�n hacia adelante del jugador
        Vector3 direction = transform.parent.forward;

        // Lanzar un rayo hacia adelante desde la posici�n del jugador
        RaycastHit hit;
        Physics.Raycast(transform.parent.position, direction, out hit);

        // Actualizar la posici�n y rotaci�n del decal
        laserDecal.transform.position = hit.point;
        laserDecal.transform.rotation = Quaternion.LookRotation(hit.normal);
    }
}