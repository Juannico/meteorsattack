using UnityEngine;
using UnityEngine.Rendering.Universal;

public class ActivarObjetoDespuesDeTiempo : MonoBehaviour
{
    public float tiempoEspera = 3f; // Tiempo en segundos antes de activar el objeto
    public DecalProjector decalProyector;

    private float tiempoInicio;
    private bool objetoActivado = false;

    private void Start()
    {
        tiempoInicio = Time.time;

        // Desactiva el Decal al principio
        decalProyector.enabled = false;
    }

    private void Update()
    {
        if (!objetoActivado && Time.time >= tiempoInicio + tiempoEspera)
        {
            objetoActivado = true;

            // Activa el Decal
            decalProyector.enabled = true;
        }
    }
}