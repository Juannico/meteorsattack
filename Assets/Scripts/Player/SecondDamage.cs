using UnityEngine;


public class PlayerScoreScript : MonoBehaviour
{
    public PlayerScore playerScore;
    public Transform playerTransform;
    public MovePlayerFireBall movementScript;

    private bool isMovedToPosition900 = false;

    private void Update()
    {
        if (playerScore.score >= 2000 && playerScore.elapsedTime >= playerScore.multipliedScoreTime && !isMovedToPosition900)
        {
            MovePlayerToPosition900Z();
        }
    }

    private void MovePlayerToPosition900Z()
    {
        if (playerTransform != null)
        {
            Vector3 newPosition = playerTransform.position;
            newPosition.z = 723f;
            playerTransform.position = newPosition;

            if (movementScript != null)
            {
                movementScript.enabled = true;
            }

            isMovedToPosition900 = true;
        }
    }
}