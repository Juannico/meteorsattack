using UnityEngine;

public class PlayerDisappearance : MonoBehaviour
{
    public int collisionCountToDisappear = 2; // N�mero de colisiones necesarias para desaparecer
    private int collisionCount = 0; // Contador de colisiones

    private bool isDestroyed = false; // Variable para verificar si el objeto ya fue destruido
    private float destructionTimer = 0f; // Temporizador para el tiempo de destrucci�n
    private float destructionkrono = 100f;

    private void Start()
    {
        destructionTimer= 0f;

   
    }
    private void Update()
    {
        // Verificar si el objeto no ha sido destruido y el temporizador ha superado los 45 segundos
        if (!isDestroyed && destructionTimer >= destructionkrono)
        {
            Destroy(gameObject);
            Debug.Log("El objeto se ha destruido despu�s de 45 segundos.");
        }

        // Incrementar el temporizador si el objeto no ha sido destruido
        if (!isDestroyed)
        {
            destructionTimer += Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        ScoreIncrease scoreIncreaseScript = other.gameObject.GetComponent<ScoreIncrease>();

        if (scoreIncreaseScript != null)
        {
            collisionCount++;

            if (collisionCount >= collisionCountToDisappear)
            {
                Destroy(gameObject);
                isDestroyed = true; // Marcar el objeto como destruido
                Debug.Log("El objeto se ha destruido debido a las colisiones.");
            }
        }
    }
}
