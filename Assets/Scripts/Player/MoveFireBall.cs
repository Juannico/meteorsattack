using UnityEngine;
using UnityEngine.InputSystem;

public class MovePlayerFireBall : MonoBehaviour
{
    public Transform targetWorld;
    public float velocity = 1f;
    public float acceleration = 1f;
    public float xyVelocity = 5f;
    public Camera cameraP;

    private float currentVelocity;
    private float elapsedSeconds;
    private bool shouldFollowTarget = true;
    private bool hasIncreasedVelocity = false;
    private InputAction moveInput;

    private void Awake()
    {
        moveInput = new InputAction("Move", binding: "<Gamepad>/rightStick");
    }

    private void OnEnable()
    {
        moveInput.Enable();
    }

    private void OnDisable()
    {
        moveInput.Disable();
    }

    private void Start()
    {
        currentVelocity = velocity;
        elapsedSeconds = 0f;
    }

    private void FixedUpdate()
    {
        if (elapsedSeconds >= 20f && !hasIncreasedVelocity)
        {
            xyVelocity += 8f;
            hasIncreasedVelocity = true;
            shouldFollowTarget = false;
        }

        Vector3 direction;
        if (shouldFollowTarget)
        {
            direction = (targetWorld.position - transform.position).normalized;
        }
        else
        {
            direction = transform.forward;
        }

        float targetMovement = currentVelocity * Time.deltaTime;
        transform.Translate(direction * targetMovement);
        currentVelocity += acceleration * Time.deltaTime;

        Vector2 move = moveInput.ReadValue<Vector2>();
        Vector3 movementObject = new Vector3(move.x, move.y, 0f) * xyVelocity * Time.deltaTime;
        Vector3 newPos = transform.position + movementObject;

        Vector3 viewportPos = cameraP.WorldToViewportPoint(newPos);
        viewportPos.x = Mathf.Clamp01(viewportPos.x);
        viewportPos.y = Mathf.Clamp01(viewportPos.y);

        transform.position = cameraP.ViewportToWorldPoint(viewportPos);

        elapsedSeconds += Time.deltaTime;
    }
}