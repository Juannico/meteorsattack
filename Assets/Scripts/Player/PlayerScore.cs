using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class PlayerScore : MonoBehaviour
{
    public int score = 1000;
    public int maxScore = 35000;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI gameOverText;
    public TextMeshProUGUI timerText;
    public Button reset;
    public Image scoreBar;
    public GameObject scoreBarFull;
    public TextMeshProUGUI newScoreText;
    public Image EndImage;
    [SerializeField] public GameObject resetcanvas;


    public float delayTime = 5f;
    public float multipliedScoreTime = 33f;

    private Coroutine multiplyCoroutine;
    private bool isScoreUpdated = false;
    private bool isMultiplied = false;
    private float startTime = 0f;
    public float elapsedTime = 0f;
    public float zeroTime = 0f;
    public float gameOverDelay = 3f;

    private void Awake()
    {
        startTime = 0;
        
     //   reset.gameObject.SetActive(false);
        
        reset.gameObject.SetActive(false);
        EndImage.gameObject.SetActive(false);
        resetcanvas.SetActive(false);   
       // newScoreText.gameObject.SetActive(false);   
    }

    private void Start()
    {
        elapsedTime = 0f;   
        zeroTime = 0f;
      
        score = 1000; // Restablecer el puntaje inicial
        isScoreUpdated = false; // Restablecer el indicador de actualización del puntaje
        isMultiplied = false; // Restablecer el indicador de multiplicación del puntaje

        if (scoreText != null)
        {

            scoreText.text = "Score: " + score.ToString();
        }
    }

    private void Update()
    {
        print(startTime);
        score = Mathf.Clamp(score, 0, int.MaxValue);
        zeroTime += Time.deltaTime;
        print(zeroTime);
        if (score <= 0)
        {
            GameOver();
        }

        if (scoreText != null)
        {
            if (!isScoreUpdated && zeroTime >= delayTime)
            {
                startTime = 0;
                isScoreUpdated = true;
            }
            if (isScoreUpdated && !isMultiplied && zeroTime >= multipliedScoreTime)
            {
                int collisionCount = ProjectileCollisionInc.GetCollisionCount();
                print("collision count: " + collisionCount);
                if (collisionCount >= 0)
                {
                    MultiplyScore(collisionCount);
                }
                else
                {
                    ShowCurrentScore();
                }
            }

            scoreText.text = "Score: " + score.ToString();

            float fillAmount = Mathf.Clamp01((float)score / maxScore);
            if (scoreBar != null)
            {
                scoreBar.fillAmount = fillAmount;
            }

            elapsedTime += Time.deltaTime;
            int minutes = Mathf.FloorToInt(elapsedTime / 60f);
            int seconds = Mathf.FloorToInt(elapsedTime % 60f);
            string timerString = string.Format("{0:00}:{1:00}", minutes, seconds);
            timerText.text = "Time: " + timerString;
        }
    }

    private void GameOver()
    {
        Debug.Log("¡Perdiste!");

        if (gameOverText != null)
        {
            gameOverText.gameObject.SetActive(true);
            EndImage.gameObject.SetActive(true);   
            gameOverText.text = "LOSE";
        }

        // Restablecer variables relevantes antes de cargar la escena
        score = 1000;
        isScoreUpdated = false;
        isMultiplied = false;
        elapsedTime = 0f; // Reiniciar el tiempo transcurrido

        StartCoroutine(ReloadSceneWithDelay(gameOverDelay));
    }

    private IEnumerator ReloadSceneWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }



    private void MultiplyScore(int collisionCount)
    {
        Debug.Log("Score final: " + score.ToString());

        if (score > 1000 && !isMultiplied)
        {
            if (multiplyCoroutine == null)
            {
                multiplyCoroutine = StartCoroutine(WaitAndMultiplyScore(collisionCount, 8f));
            }
        }
        else
        {
            PerformMultiplyScore(collisionCount);
        }
    }

    private IEnumerator WaitAndMultiplyScore(int collisionCount, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        multiplyCoroutine = null;
        PerformMultiplyScore(collisionCount);
    }

    private void PerformMultiplyScore(int collisionCount)
    {
        score *= collisionCount;
        isMultiplied = true;
        Debug.Log("Puntaje multiplicado. Nuevo puntaje: " + score.ToString());
        reset.gameObject.SetActive(true);
        resetcanvas.SetActive(true);
        if (scoreBarFull != null)
        {
            scoreBarFull.SetActive(true);
        }

      //  if (scoreText != null)
       // {
       //     scoreText.gameObject.SetActive(false);
      //  }

        if (newScoreText != null)
        {
            newScoreText.gameObject.SetActive(true);
            newScoreText.text = "Score: " + score.ToString();
        }

        if (EndImage != null)
        {
            EndImage.gameObject.SetActive(true);
        }
    }

    private void ShowCurrentScore()
    {
        Debug.Log("Score final: " + score.ToString());

        if (scoreBarFull != null)
        {
            scoreBarFull.SetActive(true);
        }

      //  if (scoreText != null)
      //  {
      //      scoreText.gameObject.SetActive(false);
      //  }

        if (newScoreText != null)
        {
            newScoreText.gameObject.SetActive(true);
            newScoreText.text = "Score: " + score.ToString();
        }

        if (EndImage != null)
        {
            EndImage.gameObject.SetActive(true);
        }

        if (score > 2000)
        {
            StartCoroutine(WaitAndShowScore(10f));
      
        }
        else
        {
            Debug.Log("Mostrar puntaje inmediatamente");
            reset.gameObject.SetActive(true);
       

        }
    }

    private IEnumerator WaitAndShowScore(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Debug.Log("Mostrar puntaje después de esperar");
        reset.gameObject.SetActive(true);
    }
}


