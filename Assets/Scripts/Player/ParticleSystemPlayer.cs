using UnityEngine;

public class ParticleSystemPlayer : MonoBehaviour
{
    public ParticleSystem firstParticleSystem;
    public ParticleSystem secondParticleSystem;
    public ParticleSystem particlePlusSystem;
    public ParticleSystem particleRestSystem;
    private bool isFirstSystemActive = true;

    private float timeParticle = 0f;

    public ParticleSystem particleSystemEffect;
    public int maxCollisions = 3;

    private int collisionCount = 0;

    private void Start()
    {
        firstParticleSystem.Play(); // Activar el primer sistema de part�culas al iniciar el juego
        particlePlusSystem.Play(); // Activar el sistema de part�culas "ParticlePlus"
        timeParticle = 0f;
        timeParticle = Time.time;   
    }

    private void Update()
    {
        if (timeParticle >= 18f && isFirstSystemActive)
        {
            firstParticleSystem.Stop(); // Desactivar el primer sistema de part�culas a los 30 segundos
            secondParticleSystem.Play(); // Activar el segundo sistema de part�culas
            isFirstSystemActive = false;
        }
    }

    public void IncreaseMaxCollisions()
    {
        maxCollisions++; // Aumentar el valor de maxCollisions
    }

    private void OnTriggerEnter(Collider other)
    {
        // Si colisiona con el tag "ZoneDamage", activa la colisi�n
        if (other.CompareTag("ZoneDamage"))
        {
            particleSystemEffect.Play();
            collisionCount++;
            Debug.Log("Explosion");
            if (collisionCount >= maxCollisions)
            {
                // -----
            }

            
        }

        // Activar sistemas de part�culas seg�n las etiquetas
        if (other.CompareTag("ParticlePlus"))
        {
            particlePlusSystem.Play(); // Activar el sistema de part�culas "ParticlePlus"
        }
        else if (other.CompareTag("Projectile"))
        {
            particleRestSystem.Play(); // Activar el sistema de part�culas "ParticleRest"
        }
    }
}
