using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolIncrease : MonoBehaviour
{
    public GameObject projectilePrefab; // Prefabricado del proyectil.
    public Transform target; // Objetivo al que se dispara.
    public float projectileSpeed = 10f; // Velocidad del proyectil.
    public int poolSize = 10; // Tama�o del grupo de objetos.
    public float minFireInterval = 1f; // Intervalo m�nimo entre disparos.
    public float maxFireInterval = 3f; // Intervalo m�ximo entre disparos.
    public float maxDeviationAngleHorizontal = 30f; // �ngulo m�ximo de desviaci�n horizontal.
    public float maxDeviationAngleVertical = 15f; // �ngulo m�ximo de desviaci�n vertical.
    public int randomInitialProjectiles = 3; // Cantidad de proyectiles iniciales colocados de manera aleatoria.

    public int timeStopShoot = 25;

    private List<GameObject> pooledProjectiles = new List<GameObject>(); // Lista de proyectiles en el grupo de objetos.
    private List<GameObject> activeProjectiles = new List<GameObject>(); // Lista de proyectiles activos.

    private bool isFiringActive = true;
    private void Start()
    {
        InitializePool(poolSize);
        StartFiringRoutine();
        FireInitialProjectiles();

     
    }

    private void InitializePool(int size)
    {
        for (int i = 0; i < size; i++)
        {
            GameObject newProjectile = Instantiate(projectilePrefab); // Instancia un nuevo proyectil a partir del prefabricado.
            newProjectile.SetActive(false); // Desactiva el proyectil.
            newProjectile.AddComponent<ProjectileCollisionInc>(); // Agrega el script ProjectileCollision al proyectil.
            pooledProjectiles.Add(newProjectile); // Agrega el proyectil a la lista de proyectiles en el grupo de objetos.
        }
    }

    private void StartFiringRoutine()
    {
        StartCoroutine(FireProjectilesPeriodically()); // Inicia la rutina de disparo peri�dico.
        StartCoroutine(StopFiringAfterTime(timeStopShoot));
    }

    private System.Collections.IEnumerator FireProjectilesPeriodically()
    {
        while (true)
        {
            if (isFiringActive)
            {
                float fireInterval = Random.Range(minFireInterval, maxFireInterval);
                yield return new WaitForSeconds(fireInterval);

                GameObject projectile = GetPooledProjectile();
                if (projectile != null)
                {
                    projectile.transform.position = transform.position;
                    projectile.transform.rotation = transform.rotation;
                    projectile.SetActive(true);

                    FireProjectile(projectile);
                }
            }
            else
            {
                yield return null;
            }
        }
    }
    private System.Collections.IEnumerator StopFiringAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        isFiringActive = false;
    }
    private void FireInitialProjectiles()
    {
        int projectilesToPlace = Mathf.Min(randomInitialProjectiles, poolSize); // Determina la cantidad de proyectiles iniciales a colocar.
        for (int i = 0; i < projectilesToPlace; i++)
        {
            GameObject projectile = GetPooledProjectile(); // Obtiene un proyectil del grupo de objetos.
            if (projectile != null)
            {
                Vector3 randomPosition = new Vector3(Random.Range(-5f, 5f), Random.Range(-5f, 5f), Random.Range(350f, 925f)); // Posici�n aleatoria en Z dentro del rango 150 - 300.
                projectile.transform.position = randomPosition; // Establece la posici�n del proyectil.
                projectile.transform.rotation = transform.rotation;
                projectile.SetActive(true); // Activa el proyectil.

                FireProjectile(projectile); // Dispara el proyectil.
            }
        }
    }

    public GameObject GetPooledProjectile()
    {
        if (pooledProjectiles.Count > 0) // Si hay proyectiles disponibles en el grupo de objetos.
        {
            GameObject projectile = pooledProjectiles[0]; // Obtiene el primer proyectil de la lista.
            pooledProjectiles.RemoveAt(0); // Elimina el proyectil de la lista de proyectiles en el grupo de objetos.
            activeProjectiles.Add(projectile); // Agrega el proyectil a la lista de proyectiles activos.
            return projectile;
        }
        else
        {
            GameObject newProjectile = Instantiate(projectilePrefab); // Instancia un nuevo proyectil a partir del prefabricado.
            newProjectile.AddComponent<ProjectileCollisionInc>(); // Agrega el script ProjectileCollision al proyectil.
            activeProjectiles.Add(newProjectile); // Agrega el proyectil a la lista de proyectiles activos.
            return newProjectile;
        }
    }

    public void ReturnProjectileToPool(GameObject projectile)
    {
        projectile.SetActive(false); // Desactiva el proyectil.
        activeProjectiles.Remove(projectile);
        pooledProjectiles.Add(projectile); // Agrega el proyectil a la lista de proyectiles en el grupo de objetos.
    }

    private void FireProjectile(GameObject projectile)
    {
        if (target != null)
        {
            Vector3 direction = (target.position - projectile.transform.position).normalized; // Calcula la direcci�n hacia el objetivo.

            // Aplica una peque�a desviaci�n angular aleatoria en ambos ejes (horizontal y vertical).
            float deviationAngleHorizontal = Random.Range(-maxDeviationAngleHorizontal, maxDeviationAngleHorizontal);
            float deviationAngleVertical = Random.Range(-maxDeviationAngleVertical, maxDeviationAngleVertical);
            Quaternion horizontalRotation = Quaternion.Euler(0f, deviationAngleHorizontal, 0f);
            Quaternion verticalRotation = Quaternion.Euler(deviationAngleVertical, 0f, 0f);
            direction = verticalRotation * horizontalRotation * direction;

            Rigidbody projectileRigidbody = projectile.GetComponent<Rigidbody>();

            if (projectileRigidbody == null) // Si el proyectil no tiene un componente Rigidbody.
            {
                Debug.LogWarning("The projectile does not have a Rigidbody component.");
                return;
            }

            projectileRigidbody.velocity = direction * projectileSpeed; // Establece la velocidad del proyectil seg�n la direcci�n y la velocidad establecida.
        }
    }
}

