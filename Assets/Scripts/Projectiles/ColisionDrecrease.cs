using UnityEngine;

public class ProjectileCollisionDec : MonoBehaviour
{
    public static int collisionCountRest = 0; // Contador de colisiones (variable est�tica)

    private ObjectPoolDecrease objectPool;
    private PlayerScore playerScore;
    public int scoreDecrease = 150;
    public float scaleDecreaseAmount = 0.1f;
    

    private void Start()
    {
        objectPool = FindObjectOfType<ObjectPoolDecrease>();
        playerScore = FindObjectOfType<PlayerScore>();
        collisionCountRest = 0;
    }

    private void OnTriggerEnter(Collider collider)
    {
        GameObject collidedObject = collider.gameObject;
        if (collidedObject.CompareTag("Player"))
        {
            collisionCountRest++; // Incrementar el contador de colisiones
            playerScore.score -= scoreDecrease;
            objectPool.ReturnProjectileToPool(gameObject);

            // Reducir la escala del jugador
            Vector3 newScale = collidedObject.transform.localScale - new Vector3(scaleDecreaseAmount, scaleDecreaseAmount, scaleDecreaseAmount);
            collidedObject.transform.localScale = newScale;
        }
    }

    // M�todo est�tico para obtener el contador de colisiones
    public static int GetCollisionCount()
    {
        return collisionCountRest;
    }
}
