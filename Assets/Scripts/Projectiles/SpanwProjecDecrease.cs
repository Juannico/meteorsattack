using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolDecrease : MonoBehaviour
{
    public GameObject projectilePrefab;
    public Transform target;
    public float projectileSpeed = 10f;
    public int poolSize = 10;
    public float minFireInterval = 1f;
    public float maxFireInterval = 3f;
    public float maxDeviationAngleHorizontal = 30f;
    public float maxDeviationAngleVertical = 15f;
    public int randomInitialProjectiles = 3;
    public int timeStopShoot = 25;

    private List<GameObject> pooledProjectiles = new List<GameObject>();
    private List<GameObject> activeProjectiles = new List<GameObject>();

    private bool isFiringActive = true;

    private void Start()
    {
        InitializePool(poolSize);
        StartFiringRoutine();
        FireInitialProjectiles();
    }

    private void InitializePool(int size)
    {
        for (int i = 0; i < size; i++)
        {
            GameObject newProjectile = Instantiate(projectilePrefab);
            newProjectile.SetActive(false);
            newProjectile.AddComponent<ProjectileCollisionDec>();
            pooledProjectiles.Add(newProjectile);
        }
    }

    private void StartFiringRoutine()
    {
        StartCoroutine(FireProjectilesPeriodically());
        StartCoroutine(StopFiringAfterTime(timeStopShoot));
    }

    private System.Collections.IEnumerator FireProjectilesPeriodically()
    {
        while (true)
        {
            if (isFiringActive)
            {
                float fireInterval = Random.Range(minFireInterval, maxFireInterval);
                yield return new WaitForSeconds(fireInterval);

                GameObject projectile = GetPooledProjectile();
                if (projectile != null)
                {
                    projectile.transform.position = transform.position;
                    projectile.transform.rotation = Quaternion.Euler(-90f, 0f, 0f); // Aplicar la rotaci�n vertical
                    projectile.SetActive(true);

                    FireProjectile(projectile);
                }
            }
            else
            {
                yield return null;
            }
        }
    }

    private System.Collections.IEnumerator StopFiringAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        isFiringActive = false;
    }

    private void FireInitialProjectiles()
    {
        int projectilesToPlace = Mathf.Min(randomInitialProjectiles, poolSize);
        for (int i = 0; i < projectilesToPlace; i++)
        {
            GameObject projectile = GetPooledProjectile();
            if (projectile != null)
            {
                Vector3 randomPosition = new Vector3(Random.Range(-5f, 5f), Random.Range(-5f, 5f), Random.Range(350f, 925f));
                projectile.transform.position = randomPosition;
                projectile.transform.rotation = Quaternion.Euler(-90f, 0f, 0f); // Aplicar la rotaci�n vertical
                projectile.SetActive(true);

                FireProjectile(projectile);
            }
        }
    }

    public GameObject GetPooledProjectile()
    {
        if (pooledProjectiles.Count > 0)
        {
            GameObject projectile = pooledProjectiles[0];
            pooledProjectiles.RemoveAt(0);
            activeProjectiles.Add(projectile);
            return projectile;
        }
        else
        {
            GameObject newProjectile = Instantiate(projectilePrefab);
            newProjectile.AddComponent<ProjectileCollisionDec>();
            activeProjectiles.Add(newProjectile);
            return newProjectile;
        }
    }

    public void ReturnProjectileToPool(GameObject projectile)
    {
        projectile.SetActive(false);
        activeProjectiles.Remove(projectile);
        pooledProjectiles.Add(projectile);
    }

    private void FireProjectile(GameObject projectile)
    {
        if (target != null)
        {
            Vector3 direction = (target.position - projectile.transform.position).normalized;

            float deviationAngleHorizontal = Random.Range(-maxDeviationAngleHorizontal, maxDeviationAngleHorizontal);
            float deviationAngleVertical = Random.Range(-maxDeviationAngleVertical, maxDeviationAngleVertical);
            Quaternion horizontalRotation = Quaternion.Euler(0f, deviationAngleHorizontal, 0f);
            Quaternion verticalRotation = Quaternion.Euler(deviationAngleVertical, 0f, 0f);
            direction = verticalRotation * horizontalRotation * direction;

            Rigidbody projectileRigidbody = projectile.GetComponent<Rigidbody>();

            if (projectileRigidbody == null)
            {
                Debug.LogWarning("The projectile does not have a Rigidbody component.");
                return;
            }

            projectileRigidbody.velocity = direction * projectileSpeed;
        }
    }
}
