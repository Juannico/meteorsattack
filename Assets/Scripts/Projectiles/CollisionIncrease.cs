using UnityEngine;

public class ProjectileCollisionInc : MonoBehaviour
{
    public static int collisionCountPlus = 0; // Contador de colisiones (variable est�tica)

    private ParticleSystemPlayer particleCounter;
    private ObjectPoolIncrease objectPool;
    private PlayerScore playerScore;
    public int scoreIncrease = 100;
    public float scaleIncreaseAmount = 0.2f;

    private void Start()
    {
        particleCounter = FindObjectOfType<ParticleSystemPlayer>();
        objectPool = FindObjectOfType<ObjectPoolIncrease>();
        playerScore = FindObjectOfType<PlayerScore>();
        collisionCountPlus = 0;
}

    private void OnTriggerEnter(Collider collider)
    {
        GameObject collidedObject = collider.gameObject;
        if (collidedObject.CompareTag("Player"))
        {
            collisionCountPlus++; // Incrementar el contador de colisiones
            Debug.Log("suma");
            playerScore.score += scoreIncrease;
            objectPool.ReturnProjectileToPool(gameObject);

            // Aumentar la escala del jugador
            Vector3 newScale = collidedObject.transform.localScale + new Vector3(scaleIncreaseAmount, scaleIncreaseAmount, scaleIncreaseAmount);
            collidedObject.transform.localScale = newScale;

            // Verificar si el contador alcanza un m�ltiplo de 2
            if (collisionCountPlus % 2 == 0)
            {
                particleCounter.IncreaseMaxCollisions(); // Aumentar el valor de maxCollisions en el segundo script
            }
        }
    }

    // M�todo est�tico para obtener el contador de colisiones
    public static int GetCollisionCount()
    {
        return collisionCountPlus;
    }
}