
using UnityEngine;
using TMPro;

public class ScoreIncrease : MonoBehaviour
{
    public int scoreIncreaseAmount = 10;
    public ParticleSystem collisionParticles; // Referencia al sistema de particulas

    private void OnTriggerEnter(Collider other)
    {
        // Comprobar si el objeto colisionado es el jugador
        PlayerScore playerScore = other.gameObject.GetComponent<PlayerScore>();
        if (playerScore != null)
        {
            // Aumentar el puntaje del jugador
            playerScore.score += scoreIncreaseAmount;
            Debug.Log("�Puntaje aumentado!");

            // Activar el sistema de particulas
            if (collisionParticles != null)
            {
                collisionParticles.Play();
            }
        }
    }
}