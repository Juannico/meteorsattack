using UnityEngine;

public class SmoothMove : MonoBehaviour
{
    public float targetPositionY = 5f; // La posici�n Y a la que queremos mover el objeto.
    public float moveDuration = 10f;   // Duraci�n del movimiento en segundos.
    public float rotateDuration = 10f;

    public GameObject worldObject;     // Referencia al objeto "WORLD" que queremos desactivar.

    private Vector3 initialPosition;   // La posici�n inicial del objeto.
    private Quaternion initialRotation; // La rotaci�n inicial del objeto.
    private float elapsedTime = 0f;    // El tiempo transcurrido desde el inicio del movimiento.
    private bool worldObjectDisabled = false; // Variable para rastrear si el objeto "WORLD" ha sido desactivado.

    private void Start()
    {
        initialPosition = transform.position; // Guardamos la posici�n inicial del objeto.
        initialRotation = transform.rotation; // Guardamos la rotaci�n inicial del objeto.
    }

    private void Update()
    {
        // Incrementamos el tiempo transcurrido desde el inicio del movimiento.
        elapsedTime += Time.deltaTime;

        // Calculamos la fracci�n del movimiento completado (entre 0 y 1).
        float t = Mathf.Clamp01(elapsedTime / moveDuration);

        // Utilizamos Lerp para obtener una posici�n suave en el eje Y.
        float newYPosition = Mathf.Lerp(initialPosition.y, targetPositionY, t);
        Vector3 newPosition = new Vector3(transform.position.x, newYPosition, transform.position.z);
        transform.position = newPosition;

        // Calculamos el �ngulo de rotaci�n.
        float rotationAngle = rotateDuration * t;

        // Rotamos el objeto utilizando la rotaci�n inicial como referencia.
        transform.rotation = initialRotation * Quaternion.Euler(-rotationAngle, 0f, 0f);

        // Verificamos si han pasado 19 segundos y el objeto "WORLD" no ha sido desactivado.
        if (elapsedTime >= 20f && !worldObjectDisabled)
        {
            worldObject.SetActive(false); // Desactivamos el objeto "WORLD".
            worldObjectDisabled = true;   // Marcamos el objeto como desactivado.
        }

        if (t >= 1f)
        {
            enabled = false;
        }
    }
}
