using UnityEngine;

public class ActivarDespuesDeTiempo : MonoBehaviour
{
    public GameObject objetoActivar;
    public float tiempoDeActivacion = 5f;

    private float tiempoTranscurrido = 0f;
    private bool activarObjeto = false;

    private void Update()
    {
        // Incrementa el tiempo transcurrido desde el inicio de la ejecuci�n
        tiempoTranscurrido += Time.deltaTime;

        // Verifica si ha pasado el tiempo de activaci�n y el objeto a�n no se ha activado
        if (tiempoTranscurrido >= tiempoDeActivacion && !activarObjeto)
        {
            // Activa el objeto y marca la bandera para que no se active nuevamente
            objetoActivar.SetActive(true);
            activarObjeto = true;
        }
    }

    private void Start()
    {
        // Desactiva el objeto al inicio de la ejecuci�n
        objetoActivar.SetActive(false);
    }
}

