using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Button play;
    public string gameSceneName;
   
    public PlayerScore playerScore;
    ProjectileCollisionInc projecInc;
    ProjectileCollisionDec projecDec;

    private void Awake()
    {
        play.gameObject.SetActive(true);
        Time.timeScale = 0f;
     
    }
    public void PlayGame()
    {
        Time.timeScale = 1f;
        play.gameObject.SetActive(false);
      //  resetcanvas.SetActive(true);
        //   playerScore.gameObject.SetActive(false);
       // playerScore.reset.gameObject.SetActive(false);
       // playerScore.EndImage.gameObject.SetActive(false);
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(gameSceneName);
        Debug.Log("resetea colision"+ ProjectileCollisionInc.collisionCountPlus);
        ProjectileCollisionInc.collisionCountPlus = 0;
        ProjectileCollisionDec.collisionCountRest = 0;

      //  playerScore.gameObject.SetActive(false);
       // playerScore.reset.gameObject.SetActive(false);
       // playerScore.EndImage.gameObject.SetActive(false);

       
      
        Time.timeScale = 0f;
    }
}

